1) Source: https://www.youtube.com/playlist?list=PLE18841CABEA24090

* 1A)
* 1B)
* 2A) Heron's method for square roots, Newton's method. Fixed points. Building layered systems. Clojures all over the place.
* 2B) Data, combining data, methodology of abstraction. Rational number representation, representing line segments, representing cons, cdr, car. 

2) Source of little Schemer: https://www.amazon.com/Little-Schemer-Daniel-P-Friedman/dp/0262560992

* 1) Toys: Enter car, cdr, cons
* 2) Again and again: Enter recursion and functions
* 3) Cons the magnificent: More about cons and functions and recursion

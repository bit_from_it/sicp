; Lecture 4A: Patterns, pattern matching

; Defining atom? that checks if an S-expression is an atom.
(define (atom? x) (not (or (pair? x) (null? x))))

; Pattern -------> Skeleton
;    |      rule       |
;    |                 |
;    |                 |
;  Expression ---> Expression target
;   source

(define deriv-rules
  '(
     ( (dd (?c c) (? v))		0 )
     ( (dd (?v v) (? v))		1 )
     ( (dd (?v u) (? v))		0 )

     ( (dd (+ (? x1) (? x2)) (? v))
       (+ (dd (: x1) (: v))
	  (dd (: x2) (: v)))	  	  )

     ( (dd (* (? x1) (? x2)) (? v))
       (+ (* (: x1) (dd (: x2) (: v)))
          (* (dd (: x1) (: v)) (: x2)))   )

     ( dd (** (? x) (?c n)) (? v))
	(* (* (: n)
		(** (: x) (: (- n 1))))
	   (dd (: x) (: v)))		  )
)

; Pattern match
;	foo 	- matches exactly foo
;	(f a b) - matches a list whose first element is f, second is a and third is b
;	(? x)	- matches anything, call it x
;	(?c x)	- matches a constant, call it x
;	(?v x)	- matches a variable, call it x
;
; Skeletons
;	foo	- instantiates to itself
;	(f a b)	- instantiates to a list of three elements, result of instantiating 
;		  each of f, a, b.
;	(: x)	- instantiate to the value of x as in the match pattern

(define dsimp
  (simplifier derivative-rules))

;In: (dsimp '(dd (+ x y) x))
;Out: (+ 1 0)

;                |----- Pattern
;                v
; ----------> Matcher ----> Dictionary
; Expression     ^
;                |--- Dictionary

(define (match pat expr dict)
  (cond ((eq? dict 'failed) 'failed)
        ((atom? pat)
        *** Atomic patterns)
       *** Pattern variable clauses
       ((atom? expr) 'failed)
       (else
         (match (cdr pat)
                (cdr expr)
                (match (car pat)
                       (car expr)
                       dict)))))

; We have to examine two trees simultaneously: the tree of the pattern and the tree of the expression 

; Atomic patterns
((atom? pat)
 (if (atom? expr)
     (if (eq? pat expr)
         dict
         'failed)
     'failed))

; Pattern variable clauses
((arbitrary-constant? pat)
 (if (constant? expr)
     (extend-dict pat expr dict)
     'failed))
((arbitrary-variable? pat)
 (if (variable? expr)
     (extend-dict pat expr dict)
     'failed))
((arbitrary-expression? pat)
 (extend-dict pat expr dict))

; Dictionary-----> Instantiate---->Expression
;                      ^---Skeleton

(define (instantiate skeleton dict)
  (define (loop s)
   (cond ((atom? s) s)
         ((skeleton-evaluation? s)
          (evaluate (eval-exp s) dict))
         ((else (cons (loop (car s))
                      (loop (cdr s)))))))
  (loop skeleton))

(define (evaluate form dict)
  (if (atom? form)
      (lookup form dict)
      (apply
        (eval (lookup (car form) dict)
              user-initial-environment)
        (mapcar (lambda (v)
                  (lookup v dict))
                (cdr form)))))

; The control structure
; Garbage in Garbage out simplifier (GIGO)
(define (simplifier the-rules)
  ; recursive traversal of expression
  (define (simplify-exp expr)
    ***)
  ; recursive traversal of expression
  (define (simplify-parts expr)
    ***)
  (define (try-rules expr)
    ***)
  simplify-exp)

(define (simplify-exp expr)
  (try-rules (if (compound? expr)
                 (simplify-parts expr)
                 expr)))

(define (simplify-parts expr)
  (if (null? expr)
    '()
   (cons (simplify-exp (car expr))
         (simplify-parts (cdr exp)))))

(define (simplify-exp expr)
  (try-rules
    (if (compound? expr)
        (map simplify-exp expr)
        expr)))

(define (try-rules expr)
  (define (scan rules)
   ***)
  (scan the-rules))

(define (scan rules)
  (if (null? rules)
      expr
      (let ((dict
             (match (pattern (car rules))
                    expr
                    (empty-dictionary))))
        (if (eq? dict 'failed)
            (scan (cdr rules))
            (simplify-exp
              (instantiate
                (skeleton (car rules))
                dict))))))

; The key to very good programming is to know
; what you don't need to think about.

(define (empty-dictionary) '())

(define (extend-dictionary pat dat dict)
  (let ((name (variable-name pat)))
   (let ((v (assq name dict)))
    (cond ((null? v)
           (cons (list name dat) dict))
          ((eq? (cadr v) dat dict)
          (else 'failed))))))

(define (lookup var dict)
  (let ((v (assq var dict)))
    (if (null? v) var (cadr v))))

(load "../interval.scm")

; TESTS
;------
(define X (make-interval 1 2))
(define Y (make-interval 2 3))
(define Z (make-interval -2 -1))
(define X1 (make-interval -1 2))
(define Y1 (make-interval 2 3))
(define Z1 (make-interval -1 0))
(define Z2 (make-interval 1 3))

; Basic
(eqv? (eq-int? X Y) #f)
(eqv? (eq-int? Y Y1) #t)

(eqv? (empty-int? X) #f)
(eqv? (empty-int? '()) #t)

(eqv? (in-int? 0 X) #f)
(eqv? (in-int? 0 Z) #f)
(eqv? (in-int? 0 X1) #t)

(eqv? (zero-int? X) #f)
(eqv? (zero-int? (make-interval 0 0)) #t)

(eqv? (included? X Z2) #t)
(eqv? (included? X Y) #f)

(intersect X Y)
(intersect X X1)
(intersect X Z)
(intersect Y Z)	; Empty intersection, no cover !

(int-union X Y)
(int-union X Z)

(int-hull X Y)
(int-hull X Z)
(int-hull X Z1)

; wid, abs, mid
(= (wid X) 1)
(= (wid Y) 1)
(= (wid X1) 3)

(= (int-abs X) 2)
(= (int-abs Z) 2)
(= (int-abs Y1) 3)

(= (mid X) 1.5)
(= (mid Y) 2.5)
(= (mid Z) -1.5)

(= (mid X) 1)

; Constants
(equal? (full-interval) '(minus-infty . infty)) ;The object (minus-infty . infty) is not applicable.
(equal? (zero-interval) '(0 . 0)) ;The object (0 . 0) is not applicable.

; Operations
(equal? (add-intervals X Y) '(3 . 5))
(add-intervals X Y)
(equal? (add-intervals X Z) '(-1 . 1))

(equal? (mul-intervals X Y) '(2 . 6))
(equal? (mul-intervals X1 Y) '(-2 . 4))
(equal? (mul-intervals Y Z) '(-6 . -2))
(equal? (mul-intervals Z X1) '(-4 . 1)) ; fails
(mul-intervals Z X1)	;(-4 . 2)

(equal? (sub-intervals X Y) '(-2 . 0))

(div-intervals X Y)
(div-intervals X X1)

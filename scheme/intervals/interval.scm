; A library of interval arithmetic operations.
; Mi.Lia
; Based on:
; a) "Interval Arithmetic: from Principles to Implementation", T. Hickey, Q. Ju, M.H. van Emden
; b) "Introduction to Interval Analysis", R. E. Moore, R. B. Kearfott, M. J. Cloud

; BASIC
;------
; Interval constructor
(define make-interval
  (lambda (a b) 
     (cons a b)))

; Testing equality between two intervals X, Y
(define eq-int?
  (lambda (X Y)
    (and (= (car X) (car Y))
     	 (= (cdr X) (cdr Y)))))   

; Testing if an interval X is the empty interval
(define empty-int?
  (lambda (X)
    (eqv? X ())))

; Testing if the number a belongs to the interval X
(define in-int?
  (lambda (a X)
    (and (> a (car X)) (< a (cdr X)))))

; Testing if the interval X is the zero interval
(define zero-int?
  (lambda (X)
    (and (= 0 (car X)) (= 0 (cdr X)))))

; Testing if the interval X is included in the interval Y (set inclusion)
(define included?
  (lambda (X Y)
    (and (<= (car Y) (car X)) (>= (cdr Y) (cdr X)))))

; Testing if intersection is empty
(define empty-inters?
  (lambda (X Y)
    (or (< (cdr Y) (car X)) (< (cdr X) (car Y)))))

; Intersection of two intervals X, Y
(define intersect
  (lambda (X Y)
    (cond
      ((empty-inters? X Y) '())
      (else
        (cons (max (car X) (car Y))
              (min (cdr X) (cdr Y)))))))

; Union of two intervals X, Y, using the general definition
; of the Interval Hull
(define int-union
  (lambda (X Y)
    (cons (min (car X) (car Y))
          (max (cdr X) (cdr Y)))))

; Interval Hull of two intervals X, Y
(define int-hull
  (lambda (X Y)
    (int-union X Y)))

; The width of an interval X
(define wid
  (lambda (X)
    (- (cdr X) (car X))))

; The absolute value of an interval X
(define int-abs
  (lambda (X)
    (max (abs (car X)) (abs (cdr X)))))

; The midpoint of an interval X
(define mid
  (lambda (X)
    (* 0.5 (+ (car X) (cdr X)))))

; CONSTANTS
;----------

(define full-interval (make-interval 'minus-infty 'infty)) ; (minus-infty . infty)
(define zero-interval (make-interval 0 0))	; (0 . 0)


; OPERATIONS
;-----------
; Adding two intervals X, Y
(define add-intervals
  (lambda (X Y)
    (cons (+ (car X) (car Y)) (+ (cdr X) (cdr Y))))) 

; Multiplying two intervals X, Y
(define mul-intervals
  (lambda (X Y)
   (cons (min (* (car X) (car Y)) 
              (* (car X) (cdr Y)) 
              (* (cdr X) (car Y))
              (* (cdr X) (cdr Y)))
         (max (* (car X) (car Y)) 
              (* (car X) (cdr Y)) 
              (* (cdr X) (car Y))
              (* (cdr X) (cdr Y))))))

; Subtracting two intervals X, Y, first version
(define sub-intervals
  (lambda (X Y)
    (cons (- (car X) (cdr Y))
          (- (cdr X) (car Y)))))

; Subtracting two intervals, X, Y, second version. Checking if X == Y.
;(define sub-intervals
;  (lambda (X Y)
;    (cond
;      ((equal? X Y) (mul-intervals (make-interval (wid X) (wid X))
;				    (make-interval -1 1))
;      (else
;       (cons (- (car X) (cdr Y))
;             (- (cdr X) (car Y))))))))


; Dividing two intervals X, Y, first version
(define div-intervals
  (lambda (X Y)
    (cond
      ((in-int? 0 Y) '())
      (else
        (mul-intervals X 
             (make-interval (/ 1 (cdr Y)) (/ 1 (car Y))))))))



; Lecture 3B: 
;  
; How to use embedding languages
; Automatic differentiation

; takes a procedure as an argument and returns a procedure as a value
; this is a representation to the numerical approximation of the derivative
(define deriv
	(lambda (f)
		(lambda (x)
			(/ (- (f (+ x dx))
				  (f x))
			dx))))

; Automatic differentiation of an expression expr of variable var
(define (deriv expr var)
	(cond ((constant? expr var) 0)
		  ((same-var? expr var) 1)
		  ((sum? expr)
		  	(make-sum (deriv (a1 expr) var)
		  		      (deriv (a2 expr) var)))
		  ((product? expr)
		  	(make-sum 
		  		(make-product (m1 expr)
		  			          (deriv (m2 expr) var))
		  		(make-product (deriv (m1 expr) var)
		  					   (m2 expr))))))

; Introduce the primitives needed

; Is it an atom?
(define (atom? x) 
  (and (not (pair? x))
       (not (null? x))))

; Is it a constant?
(define (constant? expr var)
	(and (atom? expr)
		 (not (eq? expr var))))

; Is it the same variable?
(define (same-var? expr var)
	(and (atom? expr)
		 (eq? expr var)))

; A sum is something that is atomic and begins with + symbol
(define (sum? expr)
	(and (not (atom? expr))
		  (= (car expr) '+)))

; How to make a sum
(define (make-sum a1 a2)
	(list '+ a1 a2))
(define a1 cadr)
(define a2 caddr)

; Is it a product?
(define (product? expr)
	(and (not (atom? expr))
		 (eq? (car expr) '*)))

; Make product
(define (make-product m1 m2)
	(list '* m1 m2))
(define m1 cadr)
(define m2 caddr)

; An example
; Error: Unbound variable: atom?
(define foo
	(+ ((* 'a 'x) 'b)))

(deriv foo 'x)

(define (make-sum a1 a2)
  (cond ((and (number? a1)
              (number? a2))
         (+ a1 a2))
         ((and (number? a1) (= a1 0))
          a2)
         ((and (number? a2) (= a2 0))
          a1)
         (else (list '+ a1 a2))))

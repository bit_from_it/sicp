def sum_int(a,b):
    return 0 if a > b else a + sum_int(a+1, b)

def sum_sqr(a,b):
    return 0 if a > b else a*a + sum_sqr(a+1, b)

def my_sum(term, a, mnext, b):
    return 0 if a > b else term(a) + my_sum(term, mnext(a),mnext, b)

inc = lambda x: x + 1
sqr = lambda x: x*x

def sum_int2(a,b):    
    return my_sum(lambda x: x, a, inc, b)

def sum_sqr2(a,b):
    return my_sum(sqr, a, inc, b)        

def pi_sum2(a,b):
    return my_sum(lambda i: 1./(i*(i+2)), a, lambda i: i + 4, b)

def average(x,y): return (x+y)/2.

# Doesn't work
def my_sqrt(x):
    tolerance = 0.00001
    def good_enuf(y): return y*y - x < tolerance
    def improve(y): return average(float(x)/y, y)
    def mtry(y):
        return y if good_enuf(y) else mtry(improve(y))
    return mtry(1)

# Works fine
def fixed_point(f,start):
    tolerance = 0.00001
    def close_enuf(u,v): return abs(u-v) < tolerance
    def iter(old, new):
        return new if close_enuf(old,new) else iter(new, f(new))
    return iter(start, f(start)) 

def my_sqrt2(x):
    return fixed_point(lambda y: average(float(x)/y,y), 1)
    
def my_sqrt3(x):
    return fixed_point(average_damp(lambda y: float(x)/y), 1)

def average_damp(f): 
    return lambda x: average(f(x),x)

# "Lambda is the anonymous name of something"
# average_damp = lambda f: lambda x: average(f(x),x)

def my_sqrt4(x):
    return newton(lambda y: x - sqr(y), 1)

def newton(f, guess):
    df = deriv(f)
    return fixed_point(lambda x: x - float(f(x))/df(x), guess)

def deriv(f):
    dx = 0.00001
    return lambda x: (f(x + dx) - f(x))/dx

'''
The rights and priviledges of first-class citizens

To be named by variables.
To be passed as arguments to procedures.
To be returned as values of procedures.
To be incorporated into data structures.
'''

def main():
    print(sum_int(1,10))
    print(sum_sqr(1,10))
    print(sum_int2(1,10))
    print(sum_sqr(1,10))
    print(8*pi_sum2(1,1000))
    print(my_sqrt(2))
    print(my_sqrt2(2))
    print(my_sqrt3(2))
    print(my_sqrt4(2))

if __name__ == "__main__":
    main()

'''Lecture 2B: Data, combining data, methodology of abstraction

Divorce the task of building things, from the task of implementing the parts.

Wishful thinking. 
'''
### Rational number representation
# constructor
#def make_rat(n, d):
    
# selectors
#def numer(): return n

#def denom(): return d

# methods
#def add_rat(x, y):
#    return make_rat(numer(x)*denom(y) + numer(y)*denom(x),
#            denom(x)*denom(y))

#def mul_rat(x, y):
#    return make_rat(numer(x)*numer(y), denom(x)*denom(y))

def cons(x,y): return (x,y)

def car(pair): return pair[0]

def cdr(pair): return pair[1]

def make_rat(n, d):
    return cons(n,d)

def numer(rat):
    return car(rat)

def denom(rat):
    return cdr(rat)

# methods
def add_rat(x, y):
    return make_rat(numer(x)*denom(y) + numer(y)*denom(x),
                    denom(x)*denom(y))

def mul_rat(x, y):
    return make_rat(numer(x)*numer(y), denom(x)*denom(y))

a = make_rat(1,2)
b = make_rat(1,4)
ans = add_rat(a,b)
print(numer(ans))
print(denom(ans))

def make_rat2(n, d):
    g = gcd(n,d)
    return cons(n/g, d/g)


### Representing line segments.
def make_seg(p, q): return cons(p,q)

def seg_start(s): return car(s)

def seg_end(s): return cdr(s)

def midpoint(s):
    a = seg_start(s)
    b = seg_end(s)
    return make_vector(average(xcor(a), xcor(b)),
                average(ycor(a), ycor(b)))

def length(s):
    dx = xcor(seg_end(s)) - xcor(seg_start(s))
    dy = ycor(seg_end(s)) - ycor(seg_start(s))
    return sqrt(sqr(dx) + sqr(dy))

### Axiom for pairs
#
# For any x and y
#  (car (cons x y)) is x
#  (cdr (cons x y)) is y


### An implementation of cons, car, cdr
def cons2(a, b):
    def temp(pick):
        if pick == 1: return a
        elif pick == 2: return b
    return temp

def car2(x): return x(1)

def cdr2(x): return x(2)

'''Lecture 3A: Exploring representations of vectors, line segments, lists, meta-linguistic abstraction

A language: primitives
          means of combination
          means of abstraction
'''

import lecture2b

### Representing vectors as pairs
def add_vec(vec1, vec2):
    """ Add two vectors together """
    return make_vector(xcor(v1) + xcor(v2), ycor(v1) + ycor(v2))

def scale(s, v):
    """ Scale a vector by a scalar s """
    return make_vector(s*xcor(v), s*ycor(v))

def make_vector(x1, x2):
    """ Creating a vector as a pair
    same as make_vector = cons"""
    return cons(x1, x2)

def xcor(vec):
    """ Taking the first element of a vector
    same as xcor = car """
    return car(vec)

def ycor(vec):
    """ Taking the second element of a vector
    same as ycor = cdr """
    return cdr(vec)

### Represent line segments
make_seg = cons
seg_start = car
seg_end = cdr

make_seg(make_vector(2,3),
         make_vector(5,1))


### Creating lists
cons(1,
     cons(2,
          cons(3,
               cons(4, nil))))

one_to_four = [1,2,3,4] # or one_to_four = list(range(1,5))
car(cdr(one_to_four)) # this gives 2
car(cdr(cdr(one_to_four))) # this gives 3

### Manipulating lists
def scale_list(s, alist):
    """ Scale a list by a scalar s """
    if len(alist) == 0:
        return Null
    else:
        return cons((s*car(alist)),
                       scale_list(s,cdr(alist)))

def my_map(p, alist): # shouldn't I use map Python function?
    """ Returns the list by applying p to each element """
    if len(alist) == 0: return Null
    else:
        return cons(p(car(alist)), my_map(p, l[1:]))

def scale_list2(s, l):
    """ Scale a list by a scalar s, version 2 """
    return my_map(lambda item: s*item, l)

def for_each(proc, alist): # is this equivalent of apply ?
    """ Apply an action to the elements of a list """
    if len(alist) == 0:
        print("done")
        break
    else:
        return proc(car(alist))
        for_each(proc, alist[1:])
        
### Building a language
# Peter Henderson's language

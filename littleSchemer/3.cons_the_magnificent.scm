; 		3. CONS THE MAGNIFICENT
;=========================================================

; Defining atom? that checks if an S-expression is an atom.
(define (atom? x) (not (or (pair? x) (null? x))))

; definition of rember (remove member)
(define rember
  (lambda (a lat)
    (cond
      ((null? lat) (quote ()))
      (else (cond
	      ((eq? (car lat) a) (cdr lat))
	      (else (rember a
			(cdr lat))))))))

(define a 'mint)
(define lat '(lamb chops and mint jelly))
(rember a lat)					; (lamb chops and jelly)	

(define a 'mint)
(define lat '(lamb chops and mint flavored mint jelly))
(rember a lat)					; (lamb chops and flavored mint jelly)

(define a 'toast)
(define lat '(bacon lettuce and tomato))	
(rember a lat)					; (bacon lettuce and tomato)

(define a 'cup)
(define lat '(coffee cup tea cup and hick cup))
(rember a lat)					; (coffee tea cup and hick cup)

; What does (rember a lat do)?			; It takes an atom and a lat as its arugments,
						                ; and makes a new lat with the first occurence
						                ; of the atom in the old lat removed.

(define a 'and)
(define lat '(bacon lettuce and tomato))
(rember a lat)					        ; (tomato)


; The Second Commandment: Use cons to build lists.

; working version of rember
(define rember
  (lambda (a lat)
    (cond
      ((null? lat) (quote ()))
      (else (cond
              ((eq? (car lat) a) (cdr lat))
              (else (cons (car lat)
                       (rember a
                         (cdr lat)))))))))

(define a 'and)
(define lat '(bacon lettuce and tomato))
(rember a lat)

; a simplified version of the working version
(define rember
  (lambda (a lat)
    (cond
      ((null? lat) (quote ()))
      ((eq? (car lat) a) (cdr lat))
      (else (cons (car lat)
              (rember a (cdr lat)))))))

; definition of firsts (returns the first element of a list of lists or () for empty list)
(define firsts
  (lambda (l)
    (cond
      ((null? l) (quote ()))
      (else (cons (car (car l)) (firsts (cdr l)))))))


(define l '((apple peach pumpkin)
	    (plum pear cherry)
	    (grape raisin pea)
	    (bean carrot eggplant)))
(firsts l)					; (apple plum grape bean)

(define l '((a b) (c d) (e f)))
(firsts l)					; (a c e)

(define l '())
(firsts l)					; ()

(define l '((five plums)
	    (four)
	    (eleven green oranges)))
(firsts l)					; (five four eleven)

(define l '(((five plums) four)
	    (eleven green oranges)
	    ((no) more)))
(firsts l)					; ((five plums) eleven (no))

; The Third Commandment
; When building a list describe the first typical element, and then cons it onto the natural recursion.

; definition of insertR
(define insertR
   (lambda (new old lat)
     (cond
       ((null? lat) (quote ()))
	(else
	  (cond
	    ((eq? (car lat) old) (cdr lat))
	      (else (cons (car lat)
                    (insertR new old
                      (cdr lat)))))))))


(define new 'topping)
(define old 'fudge)
(define lat '(ice cream with fudge for dessert))
(insertR new old lat)				; (ice cream with fudge topping for dessert)

(define new 'jalapeno)
(define old 'and)
(define lat '(tacos tamales and salsa))
(insertR new old lat)				; (tacos tamales and jalapeno salsa)

(define new 'e)
(define old 'd)
(define lat '(a b c d f g d h))
(insertR new old lat)				; (a b c d e f g d h)

(define new 'topping)
(define old 'fudge)
(define lat '(ice cream with fudge for dessert))
(insertR new old lat)				; (ice cream with for dessert)

(define insertR
  (lambda (new old lat)
    (cond
      ((null? lat) (quote ()))
      (else (cond
              ((eq? (car lat) old)
               (cons new (cdr lat)))
              (else (cons (car lat)
                      (insertR new old
                        (cdr lat)))))))))

(define new 'topping)
(define old 'fudge)
(define lat '(ice cream with fudge for dessert))
(insertR new old lat)				; (ice cream with topping for dessert)

(define insertR
  (lambda (new old lat)
    (cond
      ((null? lat) (quote ()))
      (else (cond
              ((eq? (car lat) old)
               (cons old
                 (cons new (cdr lat))))
              (else (cons (car lat)
                      (insertR new old
                        (cdr lat)))))))))

(define new 'topping)
(define old 'fudge)
(define lat '(ice cream with fudge for dessert))
(insertR new old lat)

(define insertL
  (lambda (new old lat)
    (cond
      ((null? lat) (quote ()))
      (else (cond
              ((eq? (car lat) old)
               (cons new
                 (cons old (cdr lat))))
              (else (cons (car lat)
                      (insertL new old
                        (cdr lat)))))))))
(define new 'topping)
(define old 'fudge)
(define lat '(ice cream with fudge for dessert))
(insertL new old lat)


; 		2. AGAIN AND AGAIN
;=========================================================

; Defining atom? that checks if an S-expression is an atom.
(define (atom? x) (not (or (pair? x) (null? x)))) ; https://stackoverflow.com/questions/5404707/check-if-an-argument-is-a-list-or-an-atom

(lat? '(Jack Sprat could eat no chicken fat))		; True because each S-expression in l is an atom.
(lat? '((Jack) Sprat could eat no chicken fat)		; False, since (car l) is a list.
(lat? '(Jack (Sprat could) eat no chicken fat)		; False, since one of the S-expressions in l is a list.
(lat? '())						; True, because it does not contain a list.
; True or false: a lat is a list of atoms.		; True !


; Defining lat? It checks if an S-expression is lat
(define lat?
  (lambda (l)
    (cond
      ((null? l) #t)
      ((atom? (car l)) (lat? (cdr l)))
      (else #f))))

; Do you remember the question about (lat? l)		; Probably not. The application (lat? l) has the value #t if the list
							; l is a list of atoms where l is (bacon and eggs)

; Can you describe what the function lat? does 		; Here are our words:
; in your own words?					; "lat? looks at each S-expression in a list, in turn, and asks if each 
							; S-expression is an atom, until it runs out of S-expressions. If it runs out
							; without encountering a list, the value is #t. If it finds a list, the value is
							; #f - false."

(lat? '(bacon (and eggs)))				; False

; Can you describe how we determined the value		; Here is one way to say it:
; #f for (lat? l) where l is (bacon (and eggs))		; "(lat? l) looks at each item in its argument to see if it is an atom. If it runs 
							; out of items before it finds a list, the value of (lat? l) is #t. If it finds a list,
							; as it did in this example, the value of (lat? l) is #f.

(eq? (							; True
         or (null? ())
            (atom? '(d e f g))
     )
     #t
)

(eq? (							; True
	or (null? '(a b c))
	   (null? ())
     )
     #t
)

(eq? (							; False
        or (null? '(a b c))
           (null? '(atom))
     )
     #t
)

; Two examples for member
(member? 'tea '(coffee tea or milk))			; True, because one of the atoms of the lat is the same as the atom tea
(member? 'poached '(fried eggs and scrambled eggs))	; False, since poached is not one of the atoms of the list

; Defining member?
(define member?
  (lambda (a lat)
    (cond
      ((null? lat) #f)
      (else (or (eq? (car lat) a)
                (member? a (cdr lat)))))))

(member? 'meat '(mashed potatoes and meat gravy))	; True

; The First Commandment (preliminary): Always ask null? as the first question in expressing any function.

(member? 'liver '(bagels and lox))			; False

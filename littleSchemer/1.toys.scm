; 			1. TOYS
;=========================================================

; Defining atom? that checks if an S-expression is an atom.
(define (atom? x) (not (or (pair? x) (null? x))))

; Checking if an expression is atomic
(atom? 'atom)	; Yes, a string of chars
(atom? 'turkey)	; Yes, a string of chars
(atom? '1492)	; Yes, a string of digits
(atom? 'u)	; Yes, a string of a char
(atom? '*abc$)	; Yes, a string of chars

; Checking if an expression is a list
(list? '(atom))			; Yes, atom enclosed by parentheses
(list? '(atom turkey or))	; Yes, collection of atoms enclosed by parentheses
(list? '(atom turkey) or)	; No, two S-expressions not enclosed by parentheses
(list? '((atom turkey) or))	; Yes

; Checking if an expression is an S-expression. How???

;(s-exp? 'xyz) 		; Yes, all atoms are S-expressions
;(s-exp? '(x y z))	; Yes, it's a list
;(s-exp '((x y) z))	; Yes
(list? '(((how) are) ((you) (doing so)) far))
(length '(((how) are) ((you) (doing so)) far)) 	;  Three.	
(list? '())		; Yes. Null (empty) list.
(atom? '())		; No, because () is just a list.
(list? '(() () () ()))  ; Yes, it's a list of S-expressions.

; car
; The Law of Car: the primitive car is defined only for non-empty lists
(car '(a b c))					; a
(car '((a b c) x y z))				; (a b c)
(car 'hotdog)					; No answer.
(car '())					; No answer, it's an empty list.
(car '(((hotdogs)) (and) (pickle) relish))	; ((hotdogs))
(car '(((hotdogs)) (and) (pickle) relish))	; ((hotdogs))
(car (car '(((hotdogs)) (and))))		; (hotdogs)

; cdr
; The Law of Cdr: the primitive cdr is defined only for non-empty lists. The cdr of any 
; 		  non-empty list is always another list.
(cdr '(a b c))		; (b,c)
(cdr '((a b c) x y z))	; (x,y,z)
(cdr '(hamburger))	; ()
(cdr '((x) t r))	; (t,r)
(cdr 'hotdogs)		; No answer.
(cdr '()) 		; No answer (nil)

(car (cdr '((b) (x y) ((c)))))	; (x,y)
(cdr (cdr '((b) (x y) ((c)))))  ; (((c)))
(cdr (car '(a (b (c)) d)))	; No answer.

; cons
; The Law of Cons: The primitive cons takes two arguments.
;		   The second argument to cons must be a list. The result is a list.
(cons 'peanut '(butter and jelly))			; (peanut butter and jelly)
(cons '(banana and) '(peanut butter and jelly))		; ((banana and) peanut butter and jelly)
(cons '((help) this) '(is very ((hard) to learn)))	; (((help) this) is very ((hard) to learn))
; What does cons take as its arguments? Cons takes two arguments: the first one is any S-expression; the second one is any list.
(cons '(a b (c)) '())					; ((a b (c)))
(cons 'a '())						; (a)
(cons '((a b c)) 'b)					; No answer.
(cons 'a 'b)						; No answer. Why?
(cons 'a (car '((b) c)))				; (a b)
(cons 'a (cdr '((b) c d)))				; (a c d)

; null?
; The Law of Null: The primitive null? is defined only for lists.
(null? (quote()))					; True
(null? '(a b c))					; False
(null? 'spaghetti)					; No answer.

(atom? 'Harry)						; True
(atom? '(Harry had a heap of apples))			; False, it's a list
(atom?  (car '(Harry had a heap of apples)))		; True
(atom?  (cdr '(Harry had a heap of apples)))		; False
(atom?  (cdr '(Harry)))					; False
(atom?  (car '(cdr (swing low sweet cherry oat))))	; True
(atom?  (car '(cdr (swing (low sweet) cherry oat))))	; False

; eq?
; The Law of Eq: The primitive eq? takes two arguments. Each must be a non-numeric atom.
(eq? 'Harry 'Harry)					; True
(eq? 'margarine 'butter)				; True
(eq? '() '(strawberry))					; No answer
(eq? '6 '7)						; No answer

(eq?  (car '(Mary had a little lamb chop) 'Mary))	; True
(eq?  (cdr '(soured milk)) 'milk)			; No answer
(eq?  (car '(beans beans we need jelly beans))
      (car  (cdr '(beans beans we need jelly beans))))	; True
